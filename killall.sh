
#!/bin/bash 


killall node-red jackd sclang reaper SatieRenderingClientApp.x86_64 ecasignalview
WEBRTC_PROCESS=`pgrep  -f satie_to_webrtc.py`
RUNSCRIPT_PROCESS=`pgrep  -f runReich.sh`

echo
echo webtrc process:  $WEBRTC_PROCESS
echo runReich process: $RUNSCRIPT_PROCESS


if [[  -z "$WEBRTC_PROCESS" ]]; then
    
	echo no satie_to_webrtc.py  found to kill
else 
    kill -09 $WEBRTC_PROCESS
fi

if [[  -z "$RUNSCRIPT_PROCESS" ]]; then
    
        echo no runReich.sh  found to kill
else 
    kill -09  $RUNSCRIPT_PROCESS
fi




exit

