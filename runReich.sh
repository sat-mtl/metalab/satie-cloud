#!/bin/bash

#  HARD CODED FOR USING SuperCollider and NOT supernova
#  Same case for the file ~/src/webrtc/satie_to_webrtc.py

mkdir -p /tmp/osm

pushd  ~/reichRendererApp


echo jackd -ddummy -r48000
jackd -R  -ddummy  -r48000 > /tmp/osm/jack.log 2>&1 &

sleep 9

echo sclang satieProject/launchReich.scd
sclang satieProject/launchReich.scd > /tmp/osm/sclang_log.log 2>&1 &

sleep 19

echo reaper \
    -cfgfile /home/ubuntu/reichRendererApp/Reaper/reaper.ini \
    /home/ubuntu/reichRendererApp/Reaper/Reich64ch_Mastered2ndTakeSpooler.rpp
    
reaper \
    -cfgfile /home/ubuntu/reichRendererApp/Reaper/reaper.ini \
    /home/ubuntu/reichRendererApp/Reaper/Reich64ch_Mastered2ndTakeSpooler.rpp > /tmp/osm/reaper.log 2>&1 &

sleep 15

echo generating jack connections using SuperCollider and not supernova

for i in $(seq 1 55);
do
    #jack_connect REAPER:out$i supernova:input_$i
    jack_connect REAPER:out$i SuperCollider:in_$i
done

echo ./SatieRenderingClientApp.x86_64 -batchmode -nographics
./SatieRenderingClientApp.x86_64 -batchmode -nographics > /tmp/osm/unity.log 2>&1 &

popd

sleep 5

echo /home/ubuntu/src/webrtc/satie_to_webrtc.py

/home/ubuntu/src/webrtc/satie_to_webrtc.py > /tmp/osm/satie2webRtc.log 2>&1 &


# sleep 8

# echo "Launch websocket server + audioPoseSync"
# pushd /home/ubuntu/src/2022-2023-osm-6dof-live/tools/
# ./websocket_server.py -i 172.31.12.248 -p 4443 -s > /tmp/osm/websocket.log 2>&1 &
# popd

# sleep 8
# echo "Connecting audioposesync to Reaper channel  64"
# jack_connect REAPER:out64 PortAudio:in_0

sleep 2

oscsend localhost 8000 /time f 0
oscsend localhost 8000 /play


sleep 1
node-red ~/src/satie-cloud/node-red/webSok2osc.json   > /tmp/osm/node-red.log 2>&1 &

sleep 5
echo ecasignalview -f:,2,48000 -i jack,SuperCollider
ecasignalview -f:,2,48000 -i jack,SuperCollider

